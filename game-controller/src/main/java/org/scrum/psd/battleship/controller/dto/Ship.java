package org.scrum.psd.battleship.controller.dto;

import java.util.ArrayList;
import java.util.List;

public class Ship {
    private boolean isPlaced;
    private String name;
    private int size;
    private List<Position> positions;
    private Color color;
    private boolean isSunk;

    public Ship() {
        this.positions = new ArrayList<>();
        this.isSunk = false;
    }

    public Ship(String name, int size) {
        this();

        this.name = name;
        this.size = size;
    }

    public Ship(String name, int size, List<Position> positions) {
        this(name, size);

        this.positions = positions;
    }

    public Ship(String name, int size, Color color) {
        this(name, size);

        this.color = color;
    }

    public boolean hasCorrectSize() {
        return positions.size() == size;
    }

    public Position createValidPosition(String input) throws Exception {
        if (positions == null) {
            positions = new ArrayList<>();
        }

        Position position = createPosition(input);

        if (positions.size() == 0) {
            return position;
        }

        for (Position p : positions) {
            if (p.equals(position)) {
                throw new Exception("The position " + position.toString() + " is already added.");
            }
        }

        if (positions.size() >= size) {
            throw new Exception("The number of positions exceeds size of a ship.");
        }

        boolean isAdjacent = false;
        for (Position p : positions) {
            boolean above = position.getColumn().equals(p.getColumn()) && position.getRow() + 1 == p.getRow();
            boolean left = (position.getColumn().ordinal() + 1 == p.getColumn().ordinal()) && position.getRow() == p.getRow();
            boolean right = (position.getColumn().ordinal() - 1 == p.getColumn().ordinal()) && position.getRow() == p.getRow();
            boolean below = position.getColumn().equals(p.getColumn()) && position.getRow() - 1 == p.getRow();

            isAdjacent = isAdjacent || above || below || left || right;
        }

        if (!isAdjacent) {
            throw new Exception("The position is not beside any other already added position.");
        }

        return position;
    }

    public boolean overlaps(Ship ship) {
        for (Position p1 : positions) {
            for (Position p2 : ship.getPositions()) {
                boolean sameRow = p1.getRow() == p2.getRow();
                boolean sameColumn = p1.getColumn().equals(p2.getColumn());
                if (sameRow && sameColumn) {
                    return true;
                }
            }
        }
        return false;
    }

    private Position createPosition(String input) throws Exception {
        Letter letter = getLetter(input);
        int number = getNumber(input);
        if (number < 1 || number > 8) {
            throw new Exception("Position you entered is invalid, please enter a valid position");
        }
        return new Position(letter, number);
    }

    private int getNumber(String input) throws Exception {
        try {
            return Integer.parseInt(input.substring(1));
        } catch (Exception e) {
            throw new Exception("Row is not an integer.", e);
        }
    }

    private Letter getLetter(String input) throws Exception {
        try {
            return Letter.valueOf(input.toUpperCase().substring(0, 1));
        } catch (Exception e) {
            throw new Exception("Column is not valid letter.", e);
        }
    }

    public void addPosition(Position position) {
        positions.add(position);
    }

    public boolean isSunk() {
        return this.isSunk;
    }

    public void registerHit(Position position) {
        // Assumption: this only gets called once we KNOW that the ship is hit

        // Register the hit on the correct position.
        for (Position pos : positions) {
            if (pos.getColumn() == position.getColumn() && pos.getRow() == position.getRow()) {
                pos.registerHit();
                break;
            }
        }

        // Check if the ship is sunk.
        this.isSunk = true;
        for (Position pos : positions) {
            if (!pos.isHit()) {
                this.isSunk = false;
                break;
            }
        }
    }

    // TODO: property change listener implementieren

    public boolean isPlaced() {
        return isPlaced;
    }

    public void setPlaced(boolean placed) {
        isPlaced = placed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Position> getPositions() {
        return positions;
    }

    public void setPositions(List<Position> positions) {
        this.positions = positions;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
}
