package org.scrum.psd.battleship.controller;

import org.junit.Assert;
import org.junit.Test;
import org.scrum.psd.battleship.controller.dto.Letter;
import org.scrum.psd.battleship.controller.dto.Position;
import org.scrum.psd.battleship.controller.dto.Ship;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GameControllerTest {
    @Test
    public void testCheckIsHitTrue() {
        List<Ship> ships = GameController.initializeShips();
        int counter = 0;

        for (Ship ship : ships) {
            Letter letter = Letter.values()[counter];

            for (int i = 0; i < ship.getSize(); i++) {
                ship.getPositions().add(new Position(letter, i));
            }

            counter++;
        }

        boolean result = GameController.checkIsHit(ships, new Position(Letter.A, 1));

        Assert.assertTrue(result);
    }

    @Test
    public void testCheckIsHitFalse() {
        List<Ship> ships = GameController.initializeShips();
        int counter = 0;

        for (Ship ship : ships) {
            Letter letter = Letter.values()[counter];

            for (int i = 0; i < ship.getSize(); i++) {
                ship.getPositions().add(new Position(letter, i));
            }

            counter++;
        }

        boolean result = GameController.checkIsHit(ships, new Position(Letter.H, 1));

        Assert.assertFalse(result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCheckIsHitPositstionIsNull() {
        GameController.checkIsHit(GameController.initializeShips(), null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCheckIsHitShipIsNull() {
        GameController.checkIsHit(null, new Position(Letter.H, 1));
    }

    @Test
    public void testIsShipValidFalse() {
        Ship ship = new Ship("TestShip", 3);
        boolean result = GameController.isShipValid(ship);

        Assert.assertFalse(result);
    }

    @Test
    public void testIsShipValidTrue() {
        List<Position> positions = Arrays.asList(new Position(Letter.A, 1), new Position(Letter.A, 1), new Position(Letter.A, 1));
        Ship ship = new Ship("TestShip", 3, positions);

        boolean result = GameController.isShipValid(ship);

        Assert.assertTrue(result);
    }

    @Test
    public void shipsOverlap() {
        List<Ship> fleet1 = new ArrayList<>(Arrays.asList(new Ship("a", 3, new ArrayList<Position>(
                Arrays.asList(
                        new Position(Letter.A, 1),
                        new Position(Letter.A, 2),
                        new Position(Letter.A, 3))
        )),
                new Ship("a", 3, new ArrayList<Position>(
                        Arrays.asList(
                                new Position(Letter.A, 1),
                                new Position(Letter.B, 1),
                                new Position(Letter.C, 1))
                        ))
                ));

        boolean overlap = GameController.checkFleetOverlapping(fleet1);

        Assert.assertTrue(overlap);
    }

    @Test
    public void fleetsNotOverlap() {
        List<Ship> fleet = new ArrayList<>(Arrays.asList(new Ship("a", 3, new ArrayList<Position>(
                        Arrays.asList(
                                new Position(Letter.A, 1),
                                new Position(Letter.A, 2),
                                new Position(Letter.A, 3))
                )),
                new Ship("a", 3, new ArrayList<Position>(
                        Arrays.asList(
                                new Position(Letter.B, 1),
                                new Position(Letter.B, 2),
                                new Position(Letter.B, 3))
                ))
        ));

        boolean overlap = GameController.checkFleetOverlapping(fleet);

        Assert.assertFalse(overlap);
    }

    @Test
    public void hasCorrectSize() {
        List<Ship> fleet = new ArrayList<>(Arrays.asList(new Ship("a", 3, new ArrayList<Position>(
                Arrays.asList(
                        new Position(Letter.A, 1),
                        new Position(Letter.A, 2),
                        new Position(Letter.A, 3))
        ))));

        boolean overlap = GameController.hasCorrectSize(fleet);

        Assert.assertTrue(overlap);
    }

    @Test
    public void morePositionsThanExpected() {
        List<Ship> fleet = new ArrayList<>(Arrays.asList(new Ship("a", 2, new ArrayList<Position>(
                Arrays.asList(
                        new Position(Letter.A, 1),
                        new Position(Letter.A, 2),
                        new Position(Letter.A, 3))
        ))));

        boolean overlap = GameController.hasCorrectSize(fleet);

        Assert.assertFalse(overlap);
    }

    @Test
    public void lessPositionsThanExpected() {
        List<Ship> fleet = new ArrayList<>(Arrays.asList(new Ship("a", 4, new ArrayList<Position>(
                Arrays.asList(
                        new Position(Letter.A, 1),
                        new Position(Letter.A, 2),
                        new Position(Letter.A, 3))
        ))));

        boolean overlap = GameController.hasCorrectSize(fleet);

        Assert.assertFalse(overlap);
    }

}
