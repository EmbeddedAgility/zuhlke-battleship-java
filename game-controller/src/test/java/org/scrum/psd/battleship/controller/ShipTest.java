package org.scrum.psd.battleship.controller;

import org.junit.Assert;
import org.junit.Test;
import org.scrum.psd.battleship.controller.dto.Letter;
import org.scrum.psd.battleship.controller.dto.Position;
import org.scrum.psd.battleship.controller.dto.Ship;

import java.util.ArrayList;
import java.util.List;

public class ShipTest {


    @Test
    public void is_letter_valid() throws Exception {
        Ship ship = new Ship();

        Position position = ship.createValidPosition("A1");

        Assert.assertTrue(position.getColumn().equals(Letter.A));
    }

    @Test
    public void adjacentPosition() throws Exception {
        Position position = new Position(Letter.A, 1);
        List<Position> positions = new ArrayList<>();
        positions.add(position);
        Ship ship = new Ship("test_name", 5, positions);

        Position p = ship.createValidPosition("B1");

        Assert.assertTrue(p.getColumn().equals(Letter.B));
        Assert.assertEquals(1, p.getRow());
    }

    @Test
    public void samePosition() throws Exception {
        Position position = new Position(Letter.A, 1);
        List<Position> positions = new ArrayList<>();
        positions.add(position);
        Ship ship = new Ship("test_name", 5, positions);

        Exception exception = Assert.assertThrows(Exception.class, () -> {
            ship.createValidPosition("A1");
        });

        String expectedMessage = "The position A1 is already added.";
        String actualMessage = exception.getMessage();

        Assert.assertEquals(expectedMessage, actualMessage);
    }

    @Test
    public void tooManyPositions() throws Exception {
        Position position = new Position(Letter.A, 1);
        List<Position> positions = new ArrayList<>();
        positions.add(position);
        Ship ship = new Ship("test_name", 1, positions);

        Exception exception = Assert.assertThrows(Exception.class, () -> {
            ship.createValidPosition("A2");
        });

        String expectedMessage = "The number of positions exceeds size of a ship.";
        String actualMessage = exception.getMessage();

        Assert.assertEquals(expectedMessage, actualMessage);
    }

    @Test
    public void notAdjacentPosition() throws Exception {
        Position position = new Position(Letter.A, 1);
        List<Position> positions = new ArrayList<>();
        positions.add(position);
        Ship ship = new Ship("test_name", 5, positions);

        Exception exception = Assert.assertThrows(Exception.class, () -> {
            ship.createValidPosition("A3");
        });

        String expectedMessage = "The position is not beside any other already added position.";
        String actualMessage = exception.getMessage();

        Assert.assertEquals(expectedMessage, actualMessage);
    }
}
