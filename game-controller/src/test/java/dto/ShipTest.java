package dto;

import org.junit.Assert;
import org.junit.Test;
import org.scrum.psd.battleship.controller.dto.Letter;
import org.scrum.psd.battleship.controller.dto.Position;
import org.scrum.psd.battleship.controller.dto.Ship;

import java.util.Collections;

public class ShipTest {

    @Test
    public void registerHitSunkTest() {
        Ship ship = new Ship("testShip", 1, Collections.singletonList(new Position(Letter.A, 1)));
        ship.registerHit(new Position(Letter.A, 1));

        Assert.assertTrue(ship.isSunk());
    }

    @Test
    public void registerHitNotSunkTest() {
        Ship ship = new Ship("testShip", 1, Collections.singletonList(new Position(Letter.A, 1)));
        ship.registerHit(new Position(Letter.A, 2));

        Assert.assertFalse(ship.isSunk());
    }
}
