package org.scrum.psd.battleship.ascii;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.scrum.psd.battleship.controller.GameController;
import org.scrum.psd.battleship.controller.dto.Letter;
import org.scrum.psd.battleship.controller.dto.Position;
import org.scrum.psd.battleship.controller.dto.Ship;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class RandomShipPositionInitializerTest {

    @Test
    public void should_create_different_positions_for_each_ship() {
        //given
        List<Ship> playerShips = GameController.initializeShips();
        List<Ship> computerShips = GameController.initializeShips();

        playerShips.get(0).getPositions().add(new Position(Letter.B, 4));
        playerShips.get(0).getPositions().add(new Position(Letter.B, 5));
        playerShips.get(0).getPositions().add(new Position(Letter.B, 6));
        playerShips.get(0).getPositions().add(new Position(Letter.B, 7));
        playerShips.get(0).getPositions().add(new Position(Letter.B, 8));

        playerShips.get(1).getPositions().add(new Position(Letter.E, 6));
        playerShips.get(1).getPositions().add(new Position(Letter.E, 7));
        playerShips.get(1).getPositions().add(new Position(Letter.E, 8));
        playerShips.get(1).getPositions().add(new Position(Letter.E, 9));

        playerShips.get(2).getPositions().add(new Position(Letter.A, 3));
        playerShips.get(2).getPositions().add(new Position(Letter.B, 3));
        playerShips.get(2).getPositions().add(new Position(Letter.C, 3));

        playerShips.get(3).getPositions().add(new Position(Letter.F, 8));
        playerShips.get(3).getPositions().add(new Position(Letter.G, 8));
        playerShips.get(3).getPositions().add(new Position(Letter.H, 8));

        playerShips.get(4).getPositions().add(new Position(Letter.C, 5));
        playerShips.get(4).getPositions().add(new Position(Letter.C, 6));

        Set<Position> playerShipPositions = new HashSet<>();

        for(Ship ship: playerShips) {
            playerShipPositions.addAll(ship.getPositions());
        }

        //when
        RandomShipPositionInitializer.initializeComputerFleetWithRandomPlaces(playerShips, computerShips);

        //then
        for(Ship ship: computerShips) {
            Assertions.assertEquals(ship.getSize(), ship.getPositions().size());
            for(Position position: ship.getPositions()) {
                Assertions.assertFalse(playerShipPositions.contains(position));
            }

        }
    }
}
