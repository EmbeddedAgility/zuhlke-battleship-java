package org.scrum.psd.battleship.ascii;

import com.diogonunes.jcdp.color.ColoredPrinter;
import com.diogonunes.jcdp.color.api.Ansi;
import org.scrum.psd.battleship.controller.GameController;
import org.scrum.psd.battleship.controller.dto.Letter;
import org.scrum.psd.battleship.controller.dto.Position;
import org.scrum.psd.battleship.controller.dto.Ship;

import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Main {
    private static List<Ship> myFleet;
    private static List<Ship> enemyFleet;
    private static ColoredPrinter console;
    private static boolean isGameOver;

    public static void main(String[] args) {
        isGameOver = false;
        console = new ColoredPrinter.Builder(1, false).background(Ansi.BColor.BLACK).foreground(Ansi.FColor.WHITE).build();

        console.setForegroundColor(Ansi.FColor.CYAN);
        console.println("                                     |__");
        console.println("                                     |\\/");
        console.println("                                     ---");
        console.println("                                     / | [");
        console.println("                              !      | |||");
        console.println("                            _/|     _/|-++'");
        console.println("                        +  +--|    |--|--|_ |-");
        console.println("                     { /|__|  |/\\__|  |--- |||__/");
        console.println("                    +---------------___[}-_===_.'____                 /\\");
        console.println("                ____`-' ||___-{]_| _[}-  |     |_[___\\==--            \\/   _");
        console.println(" __..._____--==/___]_|__|_____________________________[___\\==--____,------' .7");
        console.println("|                        Welcome to Battleship                         BB-61/");
        console.println(" \\_________________________________________________________________________|");
        console.println("");
        console.setForegroundColor(Ansi.FColor.WHITE);

        try {
            InitializeGame();
        } catch (Exception e) {
            console.errorPrint(e.getMessage());
            return;
        }

        StartGame();
    }

    private static void StartGame() {
        Scanner scanner = new Scanner(System.in);

        console.setForegroundColor(Ansi.FColor.CYAN);
        console.print("\033[2J\033[;H");
        console.println("                  __");
        console.println("                 /  \\");
        console.println("           .-.  |    |");
        console.println("   *    _.-'  \\  \\__/");
        console.println("    \\.-'       \\");
        console.println("   /          _/");
        console.println("  |      _  /\" \"");
        console.println("  |     /_\'");
        console.println("   \\    \\_/");
        console.println("    \" \"\" \"\" \"\" \"");

        do {

            boolean positionInvalid = true;
            Position position = null;
            boolean hasWon = false;

            while (positionInvalid) {
                console.setForegroundColor(Ansi.FColor.MAGENTA);
                console.println("");
                console.println("Player, it's your turn");
                console.println("Enter coordinates for your shot :");
                try {
                    String command = scanner.next();
                    if (parseSpecialCommand(command)) {
                        if (isGameOver) {
                            win();
                            hasWon = true;
                            break;
                        }
                        continue;
                    } else {
                        position = parsePosition(command);
                    }
                    positionInvalid = false;
                } catch (final IllegalArgumentException e) {
                    console.setForegroundColor(Ansi.FColor.RED);
                    console.println("Position you entered is invalid, please enter a valid position");
                }
            }
            if (hasWon) {
                break;
            }

            boolean isHit = GameController.checkIsHit(enemyFleet, position);

            boolean isSunk = false;
            for (Ship ship : enemyFleet) {
                for (Position p : ship.getPositions()) {
                    if (p.equals(position)) {
                        isSunk = ship.isSunk();
                        break;
                    }
                }
            }
            if (isHit) {
                console.setForegroundColor(Ansi.FColor.GREEN);
                console.println("HIT!");
                console.setForegroundColor(Ansi.FColor.BLUE);
                beep();

                console.println("                \\         .  ./");
                console.println("              \\      .:\" \";'.:..\" \"   /");
                console.println("                  (M^^.^~~:.'\" \").");
                console.println("            -   (/  .    . . \\ \\)  -");
                console.println("               ((| :. ~ ^  :. .|))");
                console.println("            -   (\\- |  \\ /  |  /)  -");
                console.println("                 -\\  \\     /  /-");
                console.println("                   \\  \\   /  /");
                console.setForegroundColor(Ansi.FColor.GREEN);

                if (isSunk) {
                    console.println("Congratulations you sank the ship!");
                    console.println("Enemy still has");
                    for (Ship ship : enemyFleet) {
                        if (!ship.isSunk()) {
                            console.println(ship.getName());
                        }
                    }
                }
            } else {
                console.setForegroundColor(Ansi.FColor.RED);
                console.println("Miss");
            }

            if (GameController.isFleetSunk(enemyFleet)) {
                console.setForegroundColor(Ansi.FColor.GREEN);
                console.println("");
                console.println("You are the winner!");
                win();
                break;
            }


            console.setForegroundColor(Ansi.FColor.MAGENTA);
            console.println("");
            console.println("===================================================================================");
            console.println("");
            console.println("");

            position = getRandomPosition();
            isHit = GameController.checkIsHit(myFleet, position);

            isSunk = false;
            for (Ship ship : myFleet) {
                for (Position p : ship.getPositions()) {
                    if (p.equals(position)) {
                        isSunk = ship.isSunk();
                        break;
                    }
                }
            }
            console.println("");

            if (isHit) {
                console.setForegroundColor(Ansi.FColor.RED);
                console.println(String.format("Computer shoot in %s%s and %s", position.getColumn(), position.getRow(), "hit your ship !"));

                console.setForegroundColor(Ansi.FColor.BLUE);
                beep();

                console.println("                \\         .  ./");
                console.println("              \\      .:\" \";'.:..\" \"   /");
                console.println("                  (M^^.^~~:.'\" \").");
                console.println("            -   (/  .    . . \\ \\)  -");
                console.println("               ((| :. ~ ^  :. .|))");
                console.println("            -   (\\- |  \\ /  |  /)  -");
                console.println("                 -\\  \\     /  /-");
                console.println("                   \\  \\   /  /");

                if (isSunk) {
                    console.println("Computer has sank your ship!");
                    console.println("You still have");
                    for (Ship ship : myFleet) {
                        if (!ship.isSunk()) {
                            console.println(ship.getName());
                        }
                    }
                }

            } else {
                console.setForegroundColor(Ansi.FColor.GREEN);
                console.println(String.format("Computer shoot in %s%s and %s", position.getColumn(), position.getRow(), "miss"));
            }

            if (GameController.isFleetSunk(myFleet)) {
                console.setForegroundColor(Ansi.FColor.RED);
                console.println("");
                console.println("You lost!");
                break;
            }

            console.setForegroundColor(Ansi.FColor.MAGENTA);
            console.println("");
            console.println("===================================================================================");
            console.println("");
            console.println("");

        } while (true);
    }

    private static void beep() {
        console.print("\007");
    }

    protected static Position parsePosition(String input) {
        Letter letter = Letter.valueOf(input.toUpperCase().substring(0, 1));
        int number = Integer.parseInt(input.substring(1));
        if (number > 8 || number < 1) {
            throw new IllegalArgumentException("Illegal row number");
        }
        return new Position(letter, number);
    }

    private static Position getRandomPosition() {
        int rows = 8;
        int lines = 8;
        Random random = new Random();
        Letter letter = Letter.values()[random.nextInt(lines)];
        int number = random.nextInt(rows);
        Position position = new Position(letter, number);
        return position;
    }

    private static void InitializeGame() throws Exception {
        InitializeMyFleet();

        InitializeEnemyFleet();

        boolean overlapping = GameController.checkFleetOverlapping(myFleet);
        if (overlapping) {
            throw new Exception("Ships are overlapping.");
        }

        boolean hasCorrectSize = GameController.hasCorrectSize(myFleet) &&
                GameController.hasCorrectSize(enemyFleet);
        if (!hasCorrectSize) {
            throw new Exception("Some of ships do not have correct size.");
        }
    }

    private static void InitializeMyFleet() {
        Scanner scanner = new Scanner(System.in);
        myFleet = GameController.initializeShips();

        console.setForegroundColor(Ansi.FColor.MAGENTA);
        console.println("Please position your fleet (Game board has size from A to H and 1 to 8) :");

        for (Ship ship : myFleet) {
            console.println("");
            console.println(String.format("Please enter the positions for the %s (size: %s)", ship.getName(), ship.getSize()));
            int i = 1;
            while (i <= ship.getSize()) {
                console.println(String.format("Enter position %s of %s (i.e A3):", i, ship.getSize()));

                String positionInput = scanner.next();
                Position position = null;

                try {
                    position = ship.createValidPosition(positionInput);
                    ship.addPosition(position);
                    i++;
                } catch (Exception e) {
                    console.println(e.getMessage());
                }
            }
        }
        console.setForegroundColor(Ansi.FColor.WHITE);
    }

    private static void InitializeEnemyFleet() {
        enemyFleet = GameController.initializeShips();
        RandomShipPositionInitializer.initializeComputerFleetWithRandomPlaces(myFleet, enemyFleet);

    }

    private static boolean parseSpecialCommand(final String command) {
        if (command.equalsIgnoreCase("list")) {
            listAllComputerLocations();
            return true;
        } else if (command.equalsIgnoreCase("win")) {
            isGameOver = true;
            return true;
        }
        return false;
    }

    private static void listAllComputerLocations() {
        console.println("===================================================================================");
        console.println("Enemy ship locations");
        for (Ship enemyShip : enemyFleet) {
            console.println(String.format("Positions of enemy %s", enemyShip.getName()));
            for (Position position : enemyShip.getPositions()) {
                console.print(position.getColumn().name() + position.getRow() + " ");
            }
            console.println(" ");
            console.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
            console.println(" ");
        }
    }

    private static void win() {
        console.setForegroundColor(Ansi.FColor.YELLOW);
        console.println("                             .---'::'        `---.");
        console.println("                            (::::::'              )");
        console.println("                            |`-----._______.-----'|");
        console.println("                            |              :::::::|");
        console.println("                           .|               ::::::!-.");
        console.println("                           \\|               :::::/|/");
        console.println("                            |               ::::::|");
        console.println("                            |          GG        :|");
        console.println("                            |          EZ     ::::|");
        console.println("                            |               ::::::|");
        console.println("                            |              .::::::|");
        console.println("                            J              :::::::F");
        console.println("                             \\            :::::::/");
        console.println("                              `.        .:::::::'");
        console.println("                                `-._  .::::::-'");
        console.println("                                    |  \"\"\"|\"");
        console.println("                                    |  :::|");
        console.println("                                    F   ::J");
        console.println("                                   /     ::\\ ");
        console.println("                              __.-'      :::`-.__");
        console.println("                             (_           ::::::_)");
        console.println("                               `\"\"\"---------\"\"\"'");
    }
}
