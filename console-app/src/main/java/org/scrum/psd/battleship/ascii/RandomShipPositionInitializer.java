package org.scrum.psd.battleship.ascii;

import org.scrum.psd.battleship.controller.dto.Letter;
import org.scrum.psd.battleship.controller.dto.Position;
import org.scrum.psd.battleship.controller.dto.Ship;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;

public class RandomShipPositionInitializer {

    private static final Set<Position> occupiedPositions = new HashSet<>();

    public static void initializeComputerFleetWithRandomPlaces(final List<Ship> playerShips, final List<Ship> computerShips) {
        fillAlreadyOccupiedPositions(playerShips);
        int shipIndex = 0;
        while (true) {
            int randomColumn = ThreadLocalRandom.current().nextInt(1, 8);
            int randomRow = ThreadLocalRandom.current().nextInt(1, 8);
            boolean placeVertically = ThreadLocalRandom.current().nextBoolean();

            if (isPositionOccupied(randomColumn, randomRow, computerShips.get(shipIndex).getSize(), placeVertically)) {
                continue;
            }

            for(int i = 0; i < computerShips.get(shipIndex).getSize(); i++) {
                Position position;
                if (placeVertically) {
                    position = new Position(Letter.values()[randomColumn], randomRow + i);
                } else {
                    position = new Position(Letter.values()[randomColumn + i], randomRow);
                }
                occupiedPositions.add(position);
                computerShips.get(shipIndex).getPositions().add(position);
            }

            computerShips.get(shipIndex).setPlaced(true);


            shipIndex++;

            if (shipIndex >= computerShips.size()){
                break;
            }
        }
        occupiedPositions.clear();
    }

    private static void fillAlreadyOccupiedPositions(final List<Ship> playerShips) {
        for(Ship ship: playerShips) {
            occupiedPositions.addAll(ship.getPositions());
        }
    }

    private static boolean isPositionOccupied(int column, int row, int size, boolean vertically) {
        for(int i=0; i<size; i++) {
            Position position;
            if (vertically) {
                if (row + i >7) {
                    return true;
                }
                position = new Position(Letter.values()[column], row + i);
            } else {
                if (column + i >7) {
                    return true;
                }
                position = new Position(Letter.values()[column + i], row);
            }
            if (occupiedPositions.contains(position)) {
                return true;
            }
        }
        return false;
    }

}
